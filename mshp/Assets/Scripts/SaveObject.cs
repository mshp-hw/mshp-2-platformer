using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveObject : MonoBehaviour
{
    public int number = 0;
    public GameObject spCube;


    void Update()
    {
        if(Input.GetButtonDown("Fire1")&& number > 0)
        {
            number--;
            Vector3 loc = new Vector3(gameObject.transform.position.x+1f, gameObject.transform.position.y,0 );
             Instantiate(spCube, loc, gameObject.transform.rotation);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "put_away" && Input.GetButtonDown("Fire2"))
        {
            number++;
            Destroy(collision.gameObject);
        }
    }
}
